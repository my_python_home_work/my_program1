import math

a = float(input(" enter a "))
b = float(input(" enter b "))
c = float(input(" enter c "))
print("ax^2 + bx + c = 0:")

discr = b ** 2 - 4 * a * c
print("discr D = ", discr)

if discr > 0:
    x1 = (-b + math.sqrt(discr)) / (2 * a)
    x2 = (-b - math.sqrt(discr)) / (2 * a)
    print('the equation has two different roots')

elif discr == 0:
    x = -b / (2 * a)
    print('the quadratic equation has two identical roots')

else:
    print("The quadratic equation has no roots")
